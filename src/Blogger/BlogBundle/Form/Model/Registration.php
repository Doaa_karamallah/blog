<?php

namespace Blogger\BlogBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

use Blogger\BlogBundle\Entity\Users;

class Registration
{
    /**
     * @Assert\Type(type="Blogger\BlogBundle\Entity\Users")
     * @Assert\Valid()
     */
    protected $user;

    /**
     * @Assert\NotBlank()
     * @Assert\True()
     */
    protected $termsAccepted;

    public function setUser(Users $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getTermsAccepted()
    {
        return $this->termsAccepted;
    }

    public function setTermsAccepted($termsAccepted)
    {
        $this->termsAccepted = (Boolean) $termsAccepted;
    }
}
?>