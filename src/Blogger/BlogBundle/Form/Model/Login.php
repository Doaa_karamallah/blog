<?php

namespace Blogger\BlogBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

use Blogger\BlogBundle\Entity\Users;

class Login
{

   	public $email;
	public $password;

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
}
?>