<?php

namespace Blogger\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder
		   ->add('First_name', null, array('error_bubbling' => true))
		    ->add('second_name', null, array('error_bubbling' => true))
           ->add('email', 'email', array('error_bubbling' => true))
		    ->add('password', 'repeated', array(
           'first_name'  => 'password',
           'second_name' => 'confirm',
           'type'        => 'password',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blogger\BlogBundle\Entity\Users'
        ));
    }

    public function getName()
    {
        return 'User';
    }
}
?>