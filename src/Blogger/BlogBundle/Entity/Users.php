<?php

namespace Blogger\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Blogger\BlogBundle\Entity\Users
 * 
 * @orm\Table(name="user")
 * @orm\Entity
 */
class Users  {

  /**
     * @orm\Id
     * @orm\Column(name="id", type="integer", nullable=false)
     * @orm\GeneratedValue(strategy="AUTO")
     */

	private $id;
	
	/**
	 * @var string $email
	 *
	 * @orm\Column(name="email", type="string", length=255, nullable=false)
	 * @assert\Email()
	 */
	private $email;
	/**
	 * @var string $password
	 *
	 * @orm\Column(name="password", type="string", length=32, nullable=false)
	 * @assert\NotBlank()
	 */
	private $password;
	/**
	 * @var string $firstName
	 *
	 * @orm\Column(name="first_name", type="string", length=255, nullable=false)
	 * @assert\NotBlank()
	 */
	private $firstName;
	/**
	 * @var string $secondName
	 *
	 * @orm\Column(name="second_name", type="string", length=255, nullable=false)
	 * @assert\NotBlank()
	 */
	private $secondName;
	/**
	 * @var string $createTime
	 *
	 * @orm\Column(name="create_time", type="integer", length=10, nullable=false)
	 */
	private $createTime;
	

	
	public function __construct() {
		$this->createTime = time();
		
	}
	
	/**
	 * Get id
	 *
	 * @return integer $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set email
	 *
	 * @param string $email
	 * @return Users
	 */
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Set password
	 *
	 * @param string $password
	 * @return Users
	 */
	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}

	/**
	 * Get password
	 *
	 * {@inheritdoc}
	 * 
	 * @return string $password
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * Set firstName
	 *
	 * @param string $firstName
	 * @return Users
	 */
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
		return $this;
	}

	/**
	 * Get firstName
	 *
	 * @return string $firstName
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * Set secondName
	 *
	 * @param string $secondName
	 * @return Users
	 */
	public function setSecondName($secondName) {
		$this->secondName = $secondName;
		return $this;
	}

	/**
	 * Get secondName
	 *
	 * @return string $secondName
	 */
	public function getSecondName() {
		return $this->secondName;
	}
	
	/**
	 * Set createTime
	 * 
	 * @param integer $createTime
	 * @return Users
	 */
	public function setCreateTime($createTime) {
		$this->createTime = $createTime;
		return $this;
	}
	
	/**
	 * Get createTime
	 * 
	 * @return integer $createTime
	 */
	public function getCreateTime() {
		return $this->createTime;
	}


	
	
	/**
	 * {@inheritdoc}
	 */
	public function canEdit($user) {
		if (is_numeric($user)) {
			return ($user == $this->id);
		}
		if ($user instanceof Users) {
			return ($user->id == $this->id);
		}
		
		throw new Exception('$user must be instance of Users or numberic');
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function canDelete($user) {
		if (is_numeric($user)) {
			return ($user == $this->getId());
		}
		if ($user instanceof Users) {
			return ($user->getId() == $this->getId());
		}
		
		throw new Exception('$user must be instance of Users or numberic');
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function __toString() {
		return sprintf('%s  %s', $this->getFirstName(), $this->getSecondName());
	}
}
