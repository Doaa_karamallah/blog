<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Blogger\BlogBundle\Controller\MainController as InterfaceController;
use Blogger\BlogBundle\Entity\Comments;
use Blogger\BlogBundle\Form\CommentType;

/**
 * Comment controller.
 *
 * @Route("/comment")
 */
class CommentController extends InterfaceController
{

    /**
     * Lists all Comment entities.
     *
     * @Route("/", name="comment")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($pId,$uId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BloggerBlogBundle:Comments')->findByPid($pId);
		
		

        return array(
            'comments' => $entities,
			'onuserID'=>$uId,

			
        );
    }
	public function getIdToUpdate ($postId)
	{
	
		$em = $this->getDoctrine()->getManager();
		$qb = $em->createQueryBuilder()
			   ->select(' MIN(s.createTime) AS max_score')
			   ->from('Blogger\\BlogBundle\\Entity\\Comments', 's')
			   ->where('s.pid = :postId')->setParameter('postId', $postId);
		$q = $qb->getQuery();
		$results = $q->getResult();
		return ($results[0]['max_score']);
		
	}
    /**
     * Creates a new Comment entity.
     *
     * @Route("/{userId}/{postId}}", name="comment_create")
     * @Method("POST")
     * @Template("BloggerBlogBundle:Comment:new.html.twig")
     */
    public function createAction($userId,$postId)
    {
        $entity = new Comments();
		$entity->setUcid($userId);
		$entity->setPid($postId);
        $form = $this->createCreateForm($entity);
		$request = $this->getRequest();
        $form->handleRequest($request);
        if ($form->isValid()) 
		{
			$em = $this->getDoctrine()->getManager();
		
			$user = $em->getRepository('BloggerBlogBundle:Users')->find($userId);
			
			$post = $em->getRepository('BloggerBlogBundle:Posts')->find($postId);
			$entity->setUser($user);
			$entity->setPost($post);
			
            $em->persist($entity);
			
            $em->flush();
			if($request->isXmlHttpRequest()) {
			/*
				$content = $this->forward('BloggerBlogBundle:Comment:ajax',array ('entity'=>$entity,'onuserID'=>$userId));
				$res = new Response($content);
				return $res;
				return array ('res'=>$res );
			
			*/
				 $response = new Response();
				 $output = array('success' => true, 'content' => $entity->getContent(),
								'id' => $entity->getId(), 'uId'=>$entity->getUcid(),
								'createTime'=>date ('D, M j,Y',$entity->getCreateTime()),
								'userFirstName'=>$user->getFirstName(),'userSecondName'=>$user->getSecondName(),
								'idToUpdate' => '#'.$postId );
				 $response->headers->set('Content-Type', 'application/json');
				 $response->setContent(json_encode($output));
				 return $response;
			}
            return $this->redirect($this->generateUrl('comment_show', array('id' => $entity->getId(),'onuserId'=>$userId)));
        }

        return array(
		'uId'=>$userId,
		'pId'=>$postId,
            'entity' => $entity,
            'form'   => $form->createView(),
			'idToUpdate'=>$idToUpdate.'x'.$userId,
        );
    }
	
	public function ajaxAction($entity,$onuserID)
    {
		$checkacess=$this->checkaAccess($entity->getUcid(),$onuserID);
	
        $deleteform = $this->createDeleteForm($entity->getId());

        return $this->render('BloggerBlogBundle:Comment:show.html.twig',array ('entity'=>$entity,'onuserId'=>$onuserID,'checkacess'=>$checkacess
		,'delete_form'=>$deleteform->createView()));
    }

    /**
     * Creates a form to create a Comment entity.
     *
     * @param Comment $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Comments $entity)
    {
        $form = $this->createForm(new CommentType(), $entity);

        return $form;
    }

    /**
     * Displays a form to create a new Comment entity.
     *
     * @Route("/new", name="comment_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($uId,$pId)
    {
	
        $entity = new Comments();
        $form   = $this->createCreateForm($entity);
        return array(
			'uId'=>$uId,
			'pId'=>$pId,
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Comment entity.
     *
     * @Route("/{id}/{onuserId}", name="comment_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id,$onuserId)
    {
	
	
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BloggerBlogBundle:Comments')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');
        }

		
        $deleteForm = $this->createDeleteForm($id);

        return array(
		'onuserId'=>$onuserId,
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
			'checkacess'=>$this->checkaAccess($entity->getUcid(),$onuserId),
        );
    }

    /**
     * Displays a form to edit an existing Comment entity.
     *
     * @Route("/{id}/edit/{onuserId}", name="comment_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id,$onuserId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BloggerBlogBundle:Comments')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');
        }

        $editForm = $this->createEditForm($entity,$onuserId);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Comment entity.
    *
    * @param Comment $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Comments $entity,$onuserId)
    {
        $form = $this->createForm(new CommentType(), $entity, array(
            'action' => $this->generateUrl('comment_update', array('id' => $entity->getId(),'onuserId'=>$onuserId)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Comment entity.
     *
     * @Route("/{id}/{onuserId}}", name="comment_update")
     * @Method("PUT")
     * @Template("BloggerBlogBundle:Comment:edit.html.twig")
     */
    public function updateAction(Request $request, $id,$onuserId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BloggerBlogBundle:Comments')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity,$onuserId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('posts_show', array('id' => $entity->getPid(),'onuserID'=>$onuserId)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Comment entity.
     *
     * @Route("/{id}", name="comment_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BloggerBlogBundle:Comments')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Comment entity.');
            }
			$postId=$entity->getPid();
            $em->remove($entity);
            $em->flush();
			return $this->redirect($this->generateUrl('posts_show', array('id' =>$postId )));
        }

        return $this->redirect($this->generateUrl('comment'));
    }

    /**
     * Creates a form to delete a Comment entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comment_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
	
	public function checkaAccess($commentOwner,$onlineUser)
	{
		return ($commentOwner==$onlineUser);
	
	}
}
