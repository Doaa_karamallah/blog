<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Blogger\BlogBundle\Controller\MainController as InterfaceController;
use Blogger\BlogBundle\Entity\Posts;
use Blogger\BlogBundle\Entity\Users;
use Blogger\BlogBundle\Form\PostsType;
use Blogger\BlogBundle\Controller\CommentController;


/**
 * Posts controller.
 *
 * @Route("/posts")
 */
class PostsController extends InterfaceController
{

    /**
     * Lists all Posts entities.
     *
     * @Route("/{onuserID}/index", name="posts")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($onuserID)
    {
	
	$em = $this->getDoctrine()->getManager();
		
		
		$qb = $em->createQueryBuilder()
			   ->select('p', 'u')
			   ->from('Blogger\\BlogBundle\\Entity\\Posts', 'p')
			   ->leftjoin('p.user', 'u');
		$q = $qb->getQuery();
		$results = $q->getResult();
		return (array ('posts'=> $results,
		'onuserID'=>$onuserID,             
		'home'=>true ) );
		
    }
    /**
     * Creates a new Posts entity.
     *
     * @Route("/{onuserID}/create", name="posts_create")
     * @Method("POST")
     * @Template("BloggerBlogBundle:Posts:new.html.twig")
     */
    public function createAction(Request $request,$onuserID)
    {
        $entity = new Posts();
        $form = $this->createCreateForm($entity,$onuserID);
        $form->handleRequest($request);

        if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$user = $em->find('Blogger\\blogBundle\\Entity\\Users', $onuserID);
			$entity->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();	 
			
		    

            return $this->redirect($this->generateUrl('posts_show', array('id' => $entity->getId(),'onuserID'=>$onuserID)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
	public function canEditORDelete($onlineUser,$userId)
	{
		return ($onlineUser== $userId);

	}
	
	
    /**
     * Creates a form to create a Posts entity.
     *
     * @param Posts $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Posts $entity,$onuserID)
    {
        $form = $this->createForm(new PostsType(), $entity, array(
            'action' => $this->generateUrl('posts_create',array ('onuserID'=>$onuserID)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Posts entity.
     *
     * @Route("/{onuserID}/new", name="posts_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($onuserID)
    {
        $entity = new Posts();
        $form   = $this->createCreateForm($entity,$onuserID);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
			'onuserID'=>$onuserID,
        );
    }

    /**
     * Finds and displays a Posts entity.
     *
     * @Route("/{id}/show/{onuserID}}", name="posts_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id,$onuserID,$home=false)
    {
	
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BloggerBlogBundle:Posts')->find($id);
		$canEditORDelete=$this->canEditORDelete($onuserID,$entity->getUid());
		$comments=$em->getRepository('BloggerBlogBundle:Comments')->findByPid($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Posts entity.');
        }

        $deleteForm = $this->createDeleteForm($id,$onuserID);

        return array(
			'onuserID'=>$onuserID,
			'canEditORDelete'=>$canEditORDelete,
			'comments'=>$comments, 	 	
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
			'home'=>$home,
        );
    }

    /**
     * Displays a form to edit an existing Posts entity.
     *
     * @Route("/{id}/edit/{onuserID}", name="posts_edit")
     * @Method("GET")
     * @Template("BloggerBlogBundle:Posts:edit.html.twig")
     */
    public function editAction($id,$onuserID)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BloggerBlogBundle:Posts')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Posts entity.');
        }

        $editForm = $this->createEditForm($entity,$onuserID);
        $deleteForm = $this->createDeleteForm($id,$onuserID);
        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

   /**
    * Creates a form to edit a Posts entity.
    *
    * @param Posts $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Posts $entity,$onuserID)
    {
        $form = $this->createForm(new PostsType(), $entity, array(
            'action' => $this->generateUrl('posts_update', array('id' => $entity->getId(), 'onuserID'=>$onuserID)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Posts entity.
     *
     * @Route("/{id}/update/{onuserID}", name="posts_update")
     * @Method("PUT")
     * @Template("BloggerBlogBundle:Posts:edit.html.twig")
     */
    public function updateAction(Request $request, $id,$onuserID)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BloggerBlogBundle:Posts')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Posts entity.');
        }

        $deleteForm = $this->createDeleteForm($id,$onuserID);
        $editForm = $this->createEditForm($entity,$onuserID);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('posts_show', array('id' => $id,'onuserID'=>$onuserID)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Posts entity.
     *
     * @Route("/{id}/delete/{onuserID}", name="posts_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id,$onuserID)
    {
        $form = $this->createDeleteForm($id,$onuserID);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BloggerBlogBundle:Posts')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Posts entity.');
            }

            $em->remove($entity);
            $em->flush();
			
			return $this->redirect($this->generateUrl('posts',array('onuserID'=>$onuserID)));
        }

        return $this->redirect($this->generateUrl('posts',array('onuserID'=>$onuserID)));
    }

       /**
     * Creates a form to delete a Posts entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id,$onuserID)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('posts_delete', array('id' => $id,'onuserID'=>$onuserID)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
