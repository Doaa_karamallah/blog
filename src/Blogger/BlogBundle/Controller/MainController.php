<?php


namespace Blogger\BlogBundle\Controller;

use	Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Blogger\BlogBundle\Entity\Users;

abstract class MainController extends BaseController {

	/**
	 * User
	 *
	 * @var Blog\BlogBundle\Entity\Users
	 */
	 static $user;
	
	/**
	 * set current user
	 *
	 * @return object
	 */
	public function setUser(Users $user) {
		 self::$user= $user;
	}
	/**
	 * Get current user
	 *
	 * @return object
	 */
	public function getUser() {
	
		return self::$user;
	}

}
