<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Blogger\BlogBundle\Form\Type\RegistrationType;
use Blogger\BlogBundle\Controller\MainController as InterfaceController;

use Blogger\BlogBundle\Form\Type\LoginType;
use Blogger\BlogBundle\Form\Model\Registration;
use Blogger\BlogBundle\Form\Model\Login;
use Symfony\Component\HttpFoundation\Request;
use Blogger\BlogBundle\Form\UsersLogin as UsersLoginForm;
use	Blogger\BlogBundle\Request\UsersLogin as UsersLoginRequest;

use	Blogger\BlogBundle\Entity\Users;


class UsersController extends InterfaceController
{

    /**
	 * @Route("/user/{uid}", name="_users_show", requirements={"uid" = "\d+"})
	 * @Template()
	 */
	public function showAction($uid) {
	
		$em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('BloggerBlogBundle:Users')->find($uid);
		// not found
		if (!$user) {
			throw ExceptionController::notFound('The user does not exist.');
		}
		 $deleteForm = $this->createDeleteForm($user->getId());
		
		return array(
		'user'=>$user,
		 'delete_form' => $deleteForm->createView());
	}
	/**
	 * @Route("/users/signup", name="_users_signup")
	 * @Template()
	 */
   public function signupAction() 
    {

		$registration = new Registration();
        $form = $this->createForm(new RegistrationType(), $registration, array(
            'action' => $this->generateUrl('_users_create'),
        ));

        return $this->render(
            'BloggerBlogBundle:Users:register.html.twig',
            array('form' => $form->createView())
        );
    }
	/**
	 * @Route("/users/create", name="_users_create")
	 * @Template()
	 */
	

	public function createAction(Request $request)
	{
			$em = $this->getDoctrine()->getManager();

			$form = $this->createForm(new RegistrationType(), new Registration());

			$form->handleRequest($request);

			if ($form->isValid()) {
			
				$registration = $form->getData();
				

				$em->persist($registration->getUser());
				
				$em->flush();
				$user=$registration->getUser();
				return $this->redirect(
				$this->generateUrl(('_users_show'),array('uid' => $user->getId())));
	
		}

		return $this->render(
			'BloggerBlogBundle:Users:register.html.twig',
			array('form' => $form->createView())
		);
	}
	
		/**
	 * @Route("/users/check", name="_users_check")
	 * @Template()
	 */
	 public function checkAction (Request $request)
	 
	 {
	 $em = $this->getDoctrine()->getManager();

			$form = $this->createForm(new loginType(), new login());

			$form->handleRequest($request);
			if ($form->isValid()) {
				$data = $form->getData();
				
				$user=$em->getRepository('BloggerBlogBundle:Users')->findOneBy(array ('email'=>$data->getEmail(), 'password'=> $data->getPassword()));
				if ($user)
				{
					$this->setUser($user);
					return $this->redirect($this->generateUrl('posts', array ('onuserID' => $user->getID())));
				}
				
			
			}
				
        return $this->render(
            'BloggerBlogBundle:Users:login.html.twig',
            array('form' => $form->createView())
        );
	 
	 }
	
	
	/**
	 * @Route("/users/login", name="_users_login")
	 * @Template()
	 */
	public function loginAction() {
	
	
	//return $this->render(
          // 'BloggerBlogBundle::layout.html.twig');
		$login = new Login();
        $form = $this->createForm(new LoginType(), $login , array(
            'action' => $this->generateUrl('_users_check'),
        ));

        return $this->render(
            'BloggerBlogBundle:Users:login.html.twig',
            array('form' => $form->createView())
        );
	/*
		$userRequest = new UsersLoginRequest;
		$form = UsersLoginForm::create($this->get('form.context'), 'users_login');
		
		$form->bind($this->get('request'), $userRequest);
		
		// get the error if any (works with forward and redirect -- see below)
		if ($this->get('request')->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
			$error = $this->get('request')->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
		} else {
			$error = $this->get('request')->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
		}
		if (method_exists($error, 'getMessage')) {
			$error = $error->getMessage();
		}
		
		$this->addTitle('Users', 'Login');
		return array('form' => $form, 'error' => $error);
		*/
	}
		/**
	 * @Route("/users/login", name="_users_logout")
	 * @Template()
	 */
	public function logoutAction() {
	
	
	//return $this->render(
          // 'BloggerBlogBundle::layout.html.twig');
		$login = new Login();
        $form = $this->createForm(new LoginType(), $login , array(
            'action' => $this->generateUrl('_users_check'),
        ));

        return $this->render(
            'BloggerBlogBundle:Users:login.html.twig',
            array('form' => $form->createView())
        );
	}
	
	
	/**
     * Deletes a Posts entity.
     *
     * @Route("/{id}", name="_users_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BloggerBlogBundle:Users')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Users entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('_users_signup'));
    }
	
	   /**
     * Creates a form to delete a Posts entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('_users_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
	}
?>	